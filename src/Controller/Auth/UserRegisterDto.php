<?php

namespace App\Controller\Auth;

use Symfony\Component\Validator\Constraints as Assert;

final readonly class UserRegisterDto
{
    public function __construct(
        #[Assert\NotBlank]
        public string $username,
        #[Assert\Email]
        #[Assert\NotBlank]
        public string $email,
        #[Assert\NotBlank]
        public string $password,
        #[Assert\EqualTo(propertyPath: 'password')]
        public string $confirmPassword
    ) {
    }
}
