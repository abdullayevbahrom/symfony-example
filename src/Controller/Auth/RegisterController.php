<?php

namespace App\Controller\Auth;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\SerializerInterface;

class RegisterController extends AbstractController
{
    public function __construct(/* private EntityManagerInterface $em, private UserPasswordHasherInterface $ph, */ /* private SerializerInterface $serializer */)
    {
    }

    #[Route('/api/register', name: 'auth_register', methods: ['POST'])]
    public function index(#[MapRequestPayload] UserRegisterDto $dto): Response
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'user' => $dto,
        ]);
        // dd($dto);

        /* $user = new User;
        $user->setUsername("john");
        $user->setEmail("john@gmail.com");
        $user->setPhone("998901234567");
        $hashedPassword = $this->ph->hashPassword($user, "john");
        $user->setPassword($hashedPassword);

        $this->em->persist($user);
        $this->em->flush();

        return $this->json([
            'message' => 'Welcome to your new controller!',
            'user' => $user,
        ]); */
    }
}
